import java.util.Scanner;

public class activity1 {

    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What's your First Name?");
        String fName = appScanner.nextLine().trim();

        System.out.println("What's your Last Name?");
        String lName = appScanner.nextLine().trim();

        System.out.println("Input First subject grade:");
        double fGrade = appScanner.nextDouble();

        System.out.println("Input Second subject grade:");
        double sGrade = appScanner.nextDouble();

        System.out.println("Input Third subject grade:");
        double tGrade = appScanner.nextDouble();

        int aveGrade = (int) (fGrade+sGrade+tGrade)/3;


        System.out.println("Good day, " + fName + " " + lName + "." + "\n Your Grade Average is:" + " " + aveGrade);



    }

}
