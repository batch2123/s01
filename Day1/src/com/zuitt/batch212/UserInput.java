package com.zuitt.batch212;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {

//        Scanner class - to get user input and it is imported from java.util package

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What's your name?");
//        trim() is used for removing white space before and after the words
        String myName = appScanner.nextLine().trim();

        System.out.println("User name is: " + myName);


        double myAge = appScanner.nextInt();

        System.out.println("Age of the user:" + myAge);


    }
}
