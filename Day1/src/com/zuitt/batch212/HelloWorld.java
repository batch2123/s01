package com.zuitt.batch212;

public class HelloWorld {

    public static void main(String[] args) {

        System.out.println("Hello World");

    }

//    public - Access Modifiers (Who can see this method?)
//    static - Non-access modifier (how should this method behave)
//    void - Return type (What should this method return?)
//    main - Method name (what should this method be called?)
//    String[] args - Method parameters (What is needed by the method to run)

}
